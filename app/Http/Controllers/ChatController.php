<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use App\Events\ChatMessage;
use Auth;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $messages = Message::all();

        return view('chat', compact('messages'));
    }

    public function messages(Request $request)
    {
        Message::create(['content' => $request->input('content'), 'user_id' => Auth::id()]);

        ChatMessage::dispatch($request->all());

        $messages = Message::all();
        
        return view('chat', compact('messages'));
    }
}
