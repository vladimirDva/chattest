<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsActiveEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check active email
        if (Auth::check()) {
            if (Auth::user()->status == 0) {
                Auth::logout();
                \Session::flash('activationError','First please active your account');
                return back();
            }
        }

        return $next($request);
    }
}
