@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Chat</div>
                <div class="row">
                    <?php $propsMessages = json_encode($messages); ?>
                    <chat :props-messages="{{ $propsMessages }}"></chat>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
