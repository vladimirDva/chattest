@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! <br>

                    You can chat with these people.
                </div>
                <div class="row">
                    @foreach ($users as $user)
                        <div class="card-body">
                            <div class="user-container">
                                <div class="avatar">
                                    <img src="https://png.pngtree.com/svg/20161212/f93e57629c.svg" alt="">
                                </div>
                                <div class="user-name">
                                    {{$user->name}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="chat-button">
                        <a href="{{ route('chat') }}">Chat</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
